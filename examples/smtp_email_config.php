<?php
/**
 * SMTP email config for Snap\EmailAdapter package
 * 
 * @package Snap\EmailAdapter
 * @version 11/12/2015 0.1.0
 * @since 11/12/2015 0.1.0
 */

return array(
    
    'debug' => false, // bool (make sure it is false in production)
    
    'smtp_host' => '', // Specify main and backup SMTP servers
    'smtp_username' => '', // SMTP username
    'smtp_password' => '', // SMTP password
    'smtp_port' => 465, // TCP port to connect to - note: TLS uses 587, SSL uses 465, unsecured (not recommended) uses 25
    //'smtp_encryption' => null, // set this to "ssl", "tls" or "none" if you are using a non-standard port
    
);