<?php
/**
 * Example of sending an email using the SwiftMailer adapter.
 *
 * @package Snap\EmailAdapter
 * @author Alex Fraundorf - AlexFraundorf.com - SnapProgramming.com
 * @copyright (c) 2015, Alex Fraundorf and/or Snap Programming and Development LLC
 * @version 11/12/2015 0.1.0
 * @since 11/12/2015 0.1.0
 * @license MIT https://opensource.org/licenses/MIT
 */

// report all errors
error_reporting(E_ALL);
// display errors IN DEVELOPMENT ONLY
ini_set('display_errors', 1);

// require the composer autoloader
require_once('../vendor/autoload.php');

// sample email data
$to_email = 'me@gmail.com';
$to_name = 'The receiver name'; // this can be null
$from_email = 'default'; // if "default" the smtp_username in the config file will be used otherwise enter the send from email
$from_name = 'The sender name'; // this can be null
$subject = 'this is a test message using swiftmailer - ' . time();
$message = 'This is a test message.<br><strong>It can have html!</strong><br><br>Bye';


// create the email adapter
$EmailAdapter = new \Snap\EmailAdapter\SwiftMailerAdapter(\Swift_Message::newInstance());

// get the config array
$config = (array) require('../config/smtp_email_config.php');

// send the email
$status = $EmailAdapter
    // set up for an smtp email
    ->setUpSmtp($config)
    // set the receiver (you can use this multiple times)
    ->addAddress($to_email, $to_name)
    // optional cc (you can use this multiple times)
    //->addCC($email, $name)
    // optional bcc (you can use this multiple times)
    //->addBCC($email, $name)
    // set the from 
    ->setFrom($from_email, $from_name)
    // optional reply to 
    //->setReplyTo($email, $name)
    // set the subject
    ->setSubject($subject)
    // set the message
    ->setMessage($message)
    // send it as plain text OR
    //->sendAsText();
    // send it as html
    ->sendAsHtml();
  
//exit($status);
       
if($status) {
    echo '<br /><br /><b>The email has sent!</b><br /><br />';
}
