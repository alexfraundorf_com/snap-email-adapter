<?php
/**
 * Abstract adapter holds the common properties and methods used by most implementations.
 *
 * @package Snap\EmailAdapter
 * @author Alex Fraundorf - AlexFraundorf.com - SnapProgramming.com
 * @copyright (c) 2015, Alex Fraundorf and/or Snap Programming and Development LLC
 * @version 11/12/2015 0.1.0
 * @since 11/12/2015 0.1.0
 * @license MIT https://opensource.org/licenses/MIT
 */
namespace Snap\EmailAdapter;

class AbstractAdapter {

    
    /**
     *
     * @var string holder for the default send from email address (from the config file)
     */
    protected $default_send_from_email;
        
    /**
     *
     * @var string holder for the email message
     */
    protected $message;
       
    /**
     *
     * @var bool flag for the to address
     */
    protected $to_set = false;
    
    /**
     *
     * @var bool flag for the from address
     */
    protected $from_set = false;
    
    /**
     *
     * @var bool flag for the subject
     */
    protected $subject_set = false;
    
    /**
     *
     * @var bool flag for the message
     */
    protected $message_set = false;
    
    /**
     *
     * @var string the character set to use
     */
    protected $character_set = 'UTF-8';
        
    
    
    /**
     * Sets the email message (may contain html) and returns the object to allow chaining.
     * 
     * @param string $message
     * @return \Snap\EmailAdapter\AbstractAdapter
     * @version 11/12/2015 0.1.0
     * @since 11/12/2015 0.1.0
     */
    public function setMessage($message) {
        $this->message = (string) $message;
        $this->message_set = true;
        return $this;
    }
      
    
    /**
     * Updates the character set to use and returns the object to allow chaining. The default setting is "UTF-8".
     * 
     * @param string $character_set
     * @return \Snap\EmailAdapter\AbstractAdapter
     * @version 11/12/2015 0.1.0
     * @since 11/12/2015 0.1.0
     */
    public function setCharacterSet($character_set) {
        $this->character_set = (string) $character_set;
        return $this;
    }
    
    
    /**
     * Formats an html message by escaping quotes and converting new lines to line breaks.
     * 
     * @param string $message the message to format
     * @return string the formatted message
     * @version 11/12/2015 0.1.0
     * @since 11/12/2015 0.1.0
     */
    protected function formatHtmlMessage($message) {
        // escape single and double quotes
        $message = str_replace('"', '\"', (string) $message);
        $message = str_replace("'", "\'", $message);
        // convert any new line entities to an html line break
        $message = str_replace('&amp;#10;', '<br />', $message);
        // convert any line breaks to an html line break
        $message = nl2br($message);
        // return
        return (string) $message;
    }    
    
    
    /**
     * Performs a simple validation to make sure all required settings have been set and returns true on success.  An 
     *  exception is thrown if a validation fails.
     * 
     * Note: this is not validating the settings, only that they are set.
     * 
     * @return true
     * @throws \UnexpectedValueException
     * @version 11/12/2015 0.1.0
     * @since 11/12/2015 0.1.0
     */
    protected function validateEmailSettings() {
        if(! $this->to_set) {
            throw new \UnexpectedValueException('Missing send to data. Use the addAddress(), addCC() or '
                    . 'addBCC() method.');
        }
        if(! $this->from_set) {
            throw new \UnexpectedValueException('Missing send from data. Use the setFrom() method.');
        }
        if(! $this->subject_set) {
            throw new \UnexpectedValueException('Missing email subject. Use the setSubject() method.');
        }
        if(! $this->message_set) {
            throw new \UnexpectedValueException('Missing email message. Use the setMessage() method.');
        }
        return true;        
    }

    
}
