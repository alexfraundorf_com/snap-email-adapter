<?php
/**
 * Email adapter interface.
 *
 * @package Snap\EmailAdapter
 * @author Alex Fraundorf - AlexFraundorf.com - SnapProgramming.com
 * @copyright (c) 2015, Alex Fraundorf and/or Snap Programming and Development LLC
 * @version 11/12/2015 0.1.0
 * @since 11/12/2015 0.1.0
 * @license MIT https://opensource.org/licenses/MIT
 */
namespace Snap\EmailAdapter;

interface AdapterInterface {
    
    
    /**
     * Set the from email address and optional name.  Returns the object to allow chaining.
     * 
     * @param string $address the email address
     * @param string $name the name (optional)
     * @return \Snap\EmailAdapter\AbstractAdapter
     * @throws \UnexpectedValueException
     * @throws \ErrorException
     */
    public function setFrom($address, $name = null);
    
    
    /**
     * Adds a send to email address and optional name. Returns the object to allow chaining. 
     *  This method can be used multiple times.
     * 
     * @param string $address the email address
     * @param string $name the name (optional)
     * @return \Snap\EmailAdapter\AbstractAdapter
     * @throws \ErrorException
     */
    public function addAddress($address, $name = null);
    
    
    /**
     * Adds a CC (carbon copy) email address and optional name. Returns the object to allow chaining. 
     *  This method can be used multiple times.
     * 
     * @param string $address the email address
     * @param string $name the name (optional)
     * @return \Snap\EmailAdapter\AbstractAdapter
     * @throws \ErrorException
     */
    public function addCC($address, $name = null);
    
    
    /**
     * Adds a BCC (blind carbon copy) email address and optional name. Returns the object to allow chaining. 
     *  This method can be used multiple times.
     * 
     * @param string $address the email address
     * @param string $name the name (optional)
     * @return \Snap\EmailAdapter\AbstractAdapter
     * @throws \ErrorException
     */
    public function addBCC($address, $name = null);
    
    
    /**
     * Adds a reply to email address and optional name. Returns the object to allow chaining. 
     *  If this method is not used the send from address will be the default reply to.
     *  This method can be used multiple times.
     * 
     * @param string $address the email address
     * @param string $name the name (optional)
     * @return \Snap\EmailAdapter\AbstractAdapter
     * @throws \ErrorException
     */
    public function addReplyTo($address, $name = null);
    
    
    /**
     * Sets the email subject and returns the object to allow chaining.
     * 
     * @param string $subject
     * @return \Snap\EmailAdapter\AbstractAdapter
     */
    public function setSubject($subject);
    
    
    /**
     * Sets the email message (may contain html) and returns the object to allow chaining.
     * 
     * @param string $message
     * @return \Snap\EmailAdapter\AbstractAdapter
     */
    public function setMessage($message);
    
    
    /**
     * Sends the email as html. A truthy response is returned on success and an exception is thrown on failure.
     * 
     * @return true|int 
     * @throws \UnexpectedValueException
     */
    public function sendAsHtml();
    
    
    /**
     * Sends the email as plain text. A truthy response is returned on success and an exception is thrown on failure.
     * 
     * @return true\int
     * @throws \UnexpectedValueException
     */
    public function sendAsText();

    
    /**
     * Add an attachment from a path on the filesystem. An exception is thrown if the file could not be found or read. 
     *  The object is returned to allow chaining.
     * 
     * @param string $path the path to the attachment
     * @param string $name sets the attachment name
     * @param string $content_type file extension (MIME) type
     * @return \Snap\EmailAdapter\AbstractAdapter
     * @throws \ErrorException
     */
    public function addAttachment($path, $name = null, $content_type = null);
    
    
    /**
     * Add an (inline) image attachement from a file. The object is returned to allow chaining.
     * 
     * @param string $path the path to the attachment
     * @param string $name sets the attachment name
     * @param string $content_type file extension (MIME) type
     * @return \Snap\EmailAdapter\SwiftMailerAdapter
     * @throws \ErrorException
     */
    public function addInlineImage($path, $name = null, $content_type = null);
    
    
    /**
     * Updates the character set to use and returns the object to allow chaining. The default setting is "UTF-8".
     * 
     * @param string $character_set
     * @return \Snap\EmailAdapter\AbstractAdapter
     */
    public function setCharacterSet($character_set);
    
    
}
