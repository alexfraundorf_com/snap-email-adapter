<?php
/**
 * PhpMailer email adapter.
 *
 * @package Snap\EmailAdapter
 * @author Alex Fraundorf - AlexFraundorf.com - SnapProgramming.com
 * @copyright (c) 2015, Alex Fraundorf and/or Snap Programming and Development LLC
 * @version 11/12/2015 0.1.0
 * @since 11/12/2015 0.1.0
 */
namespace Snap\EmailAdapter;

// manually load the parent class and interface so the autoloader doesn't have to
require_once('AbstractAdapter.php');
require_once('AdapterInterface.php');

class PhpMailerAdapter extends AbstractAdapter implements AdapterInterface {
    
    
    /**
     *
     * @var \PhpMailer holder for the PhpMailer object
     */
    protected $PhpMailer;
    
    
    /**
     * Constructor
     * 
     * @param \PhpMailer $PhpMailer
     * @version 11/12/2015 0.1.0
     * @since 11/12/2015 0.1.0
     */
    public function __construct(\PhpMailer $PhpMailer) {
        $this->PhpMailer = $PhpMailer;
    }
    
    
    /**
     * Sets up the email to send as SMTP and returns the object to allow chaining.
     * 
     * @param array $config the config array
     * @return \Snap\EmailAdapter\PhpMailerAdapter
     * @throws \InvalidArgumentException
     * @version 11/12/2015 0.1.0
     * @since 11/12/2015 0.1.0
     */
    public function setUpSmtp(array $config) {
        if(!$config) {
            throw new \InvalidArgumentException('Invalid config array provided.');
        }
        // make config adjustments
        if($config['debug']) {
            ini_set('display_errors', 1);
            $config['debug'] = 3; // set phpmailer to verbose debugging
        }
        // run the phpmailer smtp setup
        if($config['debug']) {
            $this->PhpMailer->SMTPDebug = $config['debug'];
        }
        // make settings
        $this->PhpMailer->isSMTP();
        $this->PhpMailer->Host = $config['smtp_host'];
        $this->PhpMailer->Username = $config['smtp_username'];
        $this->PhpMailer->Password = $config['smtp_password'];
        $this->PhpMailer->Port = $config['smtp_port'];
        // set the auth and secure settings based on the port
        if($this->PhpMailer->Port === 587){
            $this->PhpMailer->SMTPAuth = true;
            $this->PhpMailer->SMTPSecure = 'tls';
        } 
        elseif ($this->PhpMailer->Port === 465) {
            $this->PhpMailer->SMTPAuth = true;
            $this->PhpMailer->SMTPSecure = 'ssl';
        }
        else {
            $this->PhpMailer->SMTPAuth = false;
        }
        // manual encryption settings (for non-standard ports)
        if(isset($config['smtp_encryption']) && $config['smtp_encryption'] === 'ssl') {
            $this->PhpMailer->SMTPAuth = true;
            $this->PhpMailer->SMTPSecure = 'ssl';
        }
        elseif(isset($config['smtp_encryption']) && $config['smtp_encryption'] === 'tls') {
            $this->PhpMailer->SMTPAuth = true;
            $this->PhpMailer->SMTPSecure = 'tls';
        }
        elseif(isset($config['smtp_encryption']) && $config['smtp_encryption'] === 'none') {
            $this->PhpMailer->SMTPAuth = false;
        }
        // set the default send from email address
        $this->default_send_from_email = $config['smtp_username'];
        // success
        return $this;
    }

    
    /**
     * Set the from email address and optional name.  Returns the object to allow chaining.
     * 
     * @param string $address the email address
     * @param string $name the name (optional)
     * @return \Snap\EmailAdapter\PhpMailerAdapter
     * @throws \UnexpectedValueException
     * @throws \ErrorException
     * @version 11/12/2015 0.1.0
     * @since 11/12/2015 0.1.0
     */
    public function setFrom($address = 'default', $name = null) {
        // if address is "default" use the default send from
        if($address === 'default') {
            // make sure the default address is set
            if(! $this->default_send_from_email) {
                throw new \UnexpectedValueException('The default send from email is not set.');
            }
            $address = $this->default_send_from_email;
        }
        // phpmailer expects an empty string as the default
        if(!$name) {
            $name = '';
        }
        if(! $this->PhpMailer->setFrom($address, $name)) {
            throw new \ErrorException('Failed to set from address (' . $address . ').');
        }
        $this->from_set = true;
        return $this;
    }
    
    
    /**
     * Adds a send to email address and optional name. Returns the object to allow chaining. 
     *  This method can be used multiple times.
     * 
     * @param string $address the email address
     * @param string $name the name (optional)
     * @return \Snap\EmailAdapter\PhpMailerAdapter
     * @throws \ErrorException
     * @version 11/12/2015 0.1.0
     * @since 11/12/2015 0.1.0
     */
    public function addAddress($address, $name = null) {
        // phpmailer expects an empty string as the default
        if(!$name) {
            $name = '';
        }
        if(! $this->PhpMailer->addAddress($address, $name)) {
            throw new \ErrorException('Failed to add to address (' . $address . ').');
        }
        $this->to_set = true;
        return $this;
    }
    

    /**
     * Adds a CC (carbon copy) email address and optional name. Returns the object to allow chaining. 
     *  This method can be used multiple times.
     * 
     * @param string $address the email address
     * @param string $name the name (optional)
     * @return \Snap\EmailAdapter\PhpMailerAdapter
     * @throws \ErrorException
     * @version 11/12/2015 0.1.0
     * @since 11/12/2015 0.1.0
     */
    public function addCC($address, $name = null) {
        // phpmailer expects an empty string as the default
        if(!$name) {
            $name = '';
        }
        if(! $this->PhpMailer->addCC($address, $name)) {
            throw new \ErrorException('Failed to add cc address (' .  $address . ').');
        }
        $this->to_set = true;
        return $this;
    }
    
    
    /**
     * Adds a BCC (blind carbon copy) email address and optional name. Returns the object to allow chaining. 
     *  This method can be used multiple times.
     * 
     * @param string $address the email address
     * @param string $name the name (optional)
     * @return \Snap\EmailAdapter\PhpMailerAdapter
     * @throws \ErrorException
     * @version 11/12/2015 0.1.0
     * @since 11/12/2015 0.1.0
     */
    public function addBCC($address, $name = null) {
        // phpmailer expects an empty string as the default
        if(!$name) {
            $name = '';
        }
        if(! $this->PhpMailer->addBCC($address, $name)) {
            throw new \ErrorException('Failed to add bcc address (' . $address . ').');
        }
        $this->to_set = true;
        return $this;
    }
    
    
    /**
     * Adds a reply to email address and optional name. Returns the object to allow chaining. 
     *  If this method is not used the send from address will be the default reply to.
     *  This method can be used multiple times.
     * 
     * @param string $address the email address
     * @param string $name the name (optional)
     * @return \Snap\EmailAdapter\PhpMailerAdapter
     * @throws \ErrorException
     * @version 11/12/2015 0.1.0
     * @since 11/12/2015 0.1.0
     */
    public function addReplyTo($address, $name = null) {
        // phpmailer expects an empty string as the default
        if(!$name) {
            $name = '';
        }
        if(! $this->PhpMailer->addReplyTo($address, $name)) {
            throw new \ErrorException('Failed to add reply to address (' . $address . ').');
        }
        return $this;
    }
    
    
    /**
     * Sets the email subject and returns the object to allow chaining.
     * 
     * @param string $subject
     * @return \Snap\EmailAdapter\PhpMailerAdapter
     * @version 11/12/2015 0.1.0
     * @since 11/12/2015 0.1.0
     */
    public function setSubject($subject) {
        $this->PhpMailer->Subject = (string) $subject;
        $this->subject_set = true; 
        return $this;
    }
    
    
    /**
     * Sends the email as html. True is returned on success and an exception is thrown on failure.
     * 
     * @return true
     * @throws \UnexpectedValueException
     * @version 11/12/2015 0.1.0
     * @since 11/12/2015 0.1.0
     */
    public function sendAsHtml() {
        if(! $this->message) {
            throw new \UnexpectedValueException('You need to set the email message before sending. Use the '
                    . 'setMessage() method.');
        }
        // set as an html email
        $this->PhpMailer->isHTML(true);
        // set the email body as the html formatted message
        $this->PhpMailer->Body = (string) $this->formatHtmlMessage($this->message);
        // set the alt body as a text only version of the message
        $this->PhpMailer->AltBody = (string) strip_tags($this->message);
        // send it
        return $this->send();
    }
    
    
    /**
     * Sends the email as plain text. True is returned on success and an exception is thrown on failure.
     * 
     * @return true
     * @throws \UnexpectedValueException
     * @version 11/12/2015 0.1.0
     * @since 11/12/2015 0.1.0
     */
    public function sendAsText() {
        if(! $this->message) {
            throw new \UnexpectedValueException('You need to set the email message before sending. Use the '
                    . 'setMessage() method.');
        }
        // set as a plain text email
        $this->PhpMailer->isHTML(false);
        // set the email body as the plain text message
        $this->PhpMailer->Body = (string) strip_tags($this->message);
        // send it
        return $this->send();
    }
    

    /**
     * Add an attachment from a path on the filesystem. An exception is thrown if the file could not be found or read. 
     *  The object is returned to allow chaining.
     * 
     * @param string $path the path to the attachment
     * @param string $name sets the attachment name
     * @param string $content_type file extension (MIME) type
     * @return \Snap\EmailAdapter\PhpMailerAdapter
     * @throws \ErrorException
     * @version 11/12/2015 0.1.0
     * @since 11/12/2015 0.1.0
     */
    public function addAttachment($path, $name = null, $content_type = null) {
        // phpmailer expects an empty string as the default
        if(!$name) {
            $name = '';
        }
        if(!$content_type) {
            $content_type = '';
        }
        if(! $this->PhpMailer->addAttachment($path, $name, 'base64', $content_type, 'attachment')) {
            throw new \ErrorException('Failed to add attachment (' . $path . ').');
        }
        return $this;
    }
    
    
    /**
     * Add an (inline) image attachement from a file. The object is returned to allow chaining.
     * 
     * @param string $path the path to the attachment
     * @param string $name sets the attachment name
     * @param string $content_type file extension (MIME) type
     * @return \Snap\EmailAdapter\PhpMailerAdapter
     * @throws \ErrorException
     * @version 11/12/2015 0.1.0
     * @since 11/12/2015 0.1.0
     */
    public function addInlineImage($path, $name = null, $content_type = null) {
        // phpmailer expects an empty string as the default
        if(!$name) {
            $name = '';
        }
        if(!$content_type) {
            $content_type = '';
        }
        if(! $this->PhpMailer->addEmbeddedImage($path, uniqid(), $name, 'base64', $content_type, 'inline')) {
            throw new \ErrorException('Failed to embed inline image (' . $path . ').');
        }
        return $this;
    }
    
    
    /**
     * Send the email.  True is returned on success and an exception is thrown on failure.
     * 
     * @return true
     * @throws \ErrorException
     * @version 11/12/2015 0.1.0
     * @since 11/12/2015 0.1.0
     */
    protected function send() {
        // set the character set
        $this->PhpMailer->CharSet = $this->character_set;
        // validate the email settings
        $this->validateEmailSettings();
        // send it
        if(! $this->PhpMailer->send()) {
            // throw an exception with the error message
            throw new \ErrorException('Email did not send. Details: ' . $this->ErrorInfo 
                    . ' Turn on debugging in the config file for more information.');
        }
        // success
        return true;
    }
    
    
}
