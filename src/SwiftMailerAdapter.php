<?php
/**
 * SwiftMailer email adapter.
 *
 * @package Snap\EmailAdapter
 * @author Alex Fraundorf - AlexFraundorf.com - SnapProgramming.com
 * @copyright (c) 2015, Alex Fraundorf and/or Snap Programming and Development LLC
 * @version 11/12/2015 0.1.0
 * @since 11/12/2015 0.1.0
 * @license MIT https://opensource.org/licenses/MIT
 */
namespace Snap\EmailAdapter;

// manually load the parent class and interface so the autoloader doesn't have to
require_once('AbstractAdapter.php');
require_once('AdapterInterface.php');

class SwiftMailerAdapter extends AbstractAdapter implements AdapterInterface {
    
    
    /**
     *
     * @var \Swift_Message holder for the SwiftMailer message object
     */
    protected $Message;
    
    /**
     *
     * @var \Swift_Transport holder for the SwiftMailer transport object
     */
    protected $Transport;
    
    /**
     *
     * @var array holder for to addresses
     */
    protected $to_addresses = array();
    
    /**
     *
     * @var array holder for cc addresses
     */
    protected $cc_addresses = array();
    
    /**
     *
     * @var array holder for bcc addresses
     */
    protected $bcc_addresses = array();
    
    /**
     *
     * @var array holder for reply to addresses
     */
    protected $reply_to_addresses = array();
    
    /**
     *
     * @var bool debug flag
     */
    protected $debug;
    
    
    
    /**
     * Constructor - set object properties and vendor settings.
     * 
     * @param \Swift_Message $SwiftMailerMessage
     * @version 11/12/2015 0.1.0
     * @since 11/12/2015 0.1.0
     */
    public function __construct(\Swift_Message $SwiftMailerMessage) {
        $this->Message = $SwiftMailerMessage;
    }
    
    
    /**
     * Sets up the email to send as SMTP and returns the object to allow chaining.
     * 
     * @param array $config the config array
     * @return \Snap\EmailAdapter\SwiftMailerAdapter
     * @throws \InvalidArgumentException
     * @version 11/12/2015 0.1.0
     * @since 11/12/2015 0.1.0
     */
    public function setUpSmtp(array $config) {
        if(!$config) {
            throw new \InvalidArgumentException('Invalid config array provided.');
        }
        if($config['debug']) {
            ini_set('display_errors', 1);
            $this->debug = true;
        }
        // run the swiftmailer smtp setup
        if($config['smtp_port'] === 465) {
            $this->Transport = \Swift_SmtpTransport::newInstance($config['smtp_host'], $config['smtp_port'], 'ssl');        
        }
        elseif($config['smtp_port'] === 587) {
            $this->Transport = \Swift_SmtpTransport::newInstance($config['smtp_host'], $config['smtp_port'], 'tls');        
        }
        // manual encryption settings (for non-standard ports)
        elseif(isset($config['smtp_encryption']) && $config['smtp_encryption'] === 'ssl') {
            $this->Transport = \Swift_SmtpTransport::newInstance($config['smtp_host'], $config['smtp_port'], 'ssl');        
        }
        elseif(isset($config['smtp_encryption']) && $config['smtp_encryption'] === 'tls') {
            $this->Transport = \Swift_SmtpTransport::newInstance($config['smtp_host'], $config['smtp_port'], 'tls');        
        }
        // default - unencrypted
        else {
            $this->Transport = \Swift_SmtpTransport::newInstance($config['smtp_host'], $config['smtp_port']);        
        }
        $this->Transport->setUsername = $config['smtp_username'];
        $this->Transport->setPassword = $config['smtp_password'];
        // set the default send from email address
        $this->default_send_from_email = $config['smtp_username'];
        // success
        return $this;
    }

    
    /**
     * Set the from email address and optional name.  Returns the object to allow chaining.
     * 
     * @param string $address the email address
     * @param string $name the name (optional)
     * @return \Snap\EmailAdapter\SwiftMailerAdapter
     * @throws \UnexpectedValueException
     * @throws \ErrorException
     * @version 11/12/2015 0.1.0
     * @since 11/12/2015 0.1.0
     */
    public function setFrom($address = 'default', $name = null) {
        // if address is "default" use the default send from
        if($address === 'default') {
            // make sure the default address is set
            if(! $this->default_send_from_email) {
                throw new \UnexpectedValueException('The default send from email is not set.');
            }
            $address = $this->default_send_from_email;
        }
        // save the address and name
        if($name) {
            $this->Message->setFrom(array($address => $name));
        }
        else {
            $this->Message->setFrom(array($address));
        }
        // set the flag
        $this->from_set = true;
        
        return $this;
    }
    
    
    /**
     * Adds a send to email address and optional name. Returns the object to allow chaining. 
     *  This method can be used multiple times.
     * 
     * @param string $address the email address
     * @param string $name the name (optional)
     * @return \Snap\EmailAdapter\SwiftMailerAdapter
     * @throws \ErrorException
     * @version 11/12/2015 0.1.0
     * @since 11/12/2015 0.1.0
     */
    public function addAddress($address, $name = null) {
        // save the address and name
        if(!$name) {
            $this->to_addresses[] = $address;
        }
        else {
            $this->to_addresses[$address] = $name;
        }
        // set the flag
        $this->to_set = true;
        
        return $this;
    }
    

    /**
     * Adds a CC (carbon copy) email address and optional name. Returns the object to allow chaining. 
     *  This method can be used multiple times.
     * 
     * @param string $address the email address
     * @param string $name the name (optional)
     * @return \Snap\EmailAdapter\SwiftMailerAdapter
     * @throws \ErrorException
     * @version 11/12/2015 0.1.0
     * @since 11/12/2015 0.1.0
     */
    public function addCC($address, $name = null) {
        // save the address and name
        if(!$name) {
            $this->cc_addresses[] = $address;
        }
        else {
            $this->cc_addresses[$address] = $name;
        }
        // set the flag
        $this->to_set = true;
        
        return $this;
    }
    
    
    /**
     * Adds a BCC (blind carbon copy) email address and optional name. Returns the object to allow chaining. 
     *  This method can be used multiple times.
     * 
     * @param string $address the email address
     * @param string $name the name (optional)
     * @return \Snap\EmailAdapter\SwiftMailerAdapter
     * @throws \ErrorException
     * @version 11/12/2015 0.1.0
     * @since 11/12/2015 0.1.0
     */
    public function addBCC($address, $name = null) {
        // save the address and name
        if(!$name) {
            $this->bcc_addresses[] = $address;
        }
        else {
            $this->bcc_addresses[$address] = $name;
        }
        // set the flag
        $this->to_set = true;
        
        return $this;
    }
    
    
    /**
     * Adds a reply to email address and optional name. Returns the object to allow chaining. 
     *  If this method is not used the send from address will be the default reply to.
     *  This method can be used multiple times.
     * 
     * @param string $address the email address
     * @param string $name the name (optional)
     * @return \Snap\EmailAdapter\SwiftMailerAdapter
     * @throws \ErrorException
     * @version 11/12/2015 0.1.0
     * @since 11/12/2015 0.1.0
     */
    public function addReplyTo($address, $name = null) {
        // save the address and name
        if(!$name) {
            $this->reply_to_addresses[] = $address;
        }
        else {
            $this->reply_to_addresses[$address] = $name;
        }

        return $this;
    }
    
    
    /**
     * Sets the email subject and returns the object to allow chaining.
     * 
     * @param string $subject
     * @return \Snap\EmailAdapter\SwiftMailerAdapter
     * @version 11/12/2015 0.1.0
     * @since 11/12/2015 0.1.0
     */
    public function setSubject($subject) {
        $this->Message->setSubject((string) $subject);        
        $this->subject_set = true;
        return $this;
    }
    
    
    /**
     * Sends the email as html. True is returned on success and an exception is thrown on failure.
     * 
     * @return true
     * @throws \UnexpectedValueException
     * @version 11/12/2015 0.1.0
     * @since 11/12/2015 0.1.0
     */
    public function sendAsHtml() {
        if(! $this->message) {
            throw new \UnexpectedValueException('You need to set the email message before sending. Use the '
                    . 'setMessage() method.');
        }
        // set as an html email
        $this->Message->setContentType('text/html');
        // set the email body as the plain text message
        $this->Message->setBody((string) strip_tags($this->message));
        // set the alt body as the html
        $this->Message->addPart((string) $this->formatHtmlMessage($this->message), 'text/html');
        // send it
        return $this->send();
    }
    
    
    /**
     * Sends the email as plain text. True is returned on success and an exception is thrown on failure.
     * 
     * @return true
     * @throws \UnexpectedValueException
     * @version 11/12/2015 0.1.0
     * @since 11/12/2015 0.1.0
     */
    public function sendAsText() {
        if(! $this->message) {
            throw new \UnexpectedValueException('You need to set the email message before sending. Use the '
                    . 'setMessage() method.');
        }
        $this->Message->setContentType('text/plain');
        // set the email body as the plain text message
        $this->Message->setBody((string) strip_tags($this->message));
        // send it
        return $this->send();
    }
    

    /**
     * Add an attachment from a path on the filesystem. An exception is thrown if the file could not be found or read. 
     *  The object is returned to allow chaining.
     * 
     * @param string $path the path to the attachment
     * @param string $name sets the attachment name
     * @param string $content_type file extension (MIME) type
     * @return \Snap\EmailAdapter\SwiftMailerAdapter
     * @throws \ErrorException
     * @version 11/12/2015 0.1.0
     * @since 11/12/2015 0.1.0
     */
    public function addAttachment($path, $name = null, $content_type = null) {
        try {
            $Attachment = Swift_Attachment::fromPath($path)->setDisposition('attachment');
            if($name) {
                $Attachment->setFileName($name);
            }
            if($content_type) {
                $Attachment->setContentType('application/pdf');
            }
            $this->Message->attach($Attachment);
        } catch (\Exception $ex) {
            throw new \ErrorException('Failed to embed inline image (' . $path . '). Details: ' . $ex->getMessage());
        }
        return $this;
    }
    
    
    /**
     * Add an (inline) image attachement from a file. The object is returned to allow chaining.
     * 
     * @param string $path the path to the attachment
     * @param string $name sets the attachment name
     * @param string $content_type file extension (MIME) type
     * @return \Snap\EmailAdapter\SwiftMailerAdapter
     * @throws \ErrorException
     * @version 11/12/2015 0.1.0
     * @since 11/12/2015 0.1.0
     */
    public function addInlineImage($path, $name = null, $content_type = null) {
        try {
            $Attachment = Swift_Attachment::fromPath($path)->setDisposition('inline');
            if($name) {
                $Attachment->setFileName($name);
            }
            if($content_type) {
                $Attachment->setContentType('application/pdf');
            }
            $this->Message->attach($Attachment);
        } catch (\Exception $ex) {
            throw new \ErrorException('Failed to embed inline image (' . $path . '). Details: ' . $ex->getMessage());
        }
        return $this;
    }
    
    
    /**
     * Send the email.  The number of emails sent is returned on success and an exception is thrown on failure.
     * 
     * @return int the number of emails sent
     * @throws \ErrorException
     * @version 11/12/2015 0.1.0
     * @since 11/12/2015 0.1.0
     */
    protected function send() {
        // set the character set
        $this->Message->setCharset($this->character_set);  
        // process the addresses and set them on the message object
        $this->processAddresses();
        // validate the email settings
        $this->validateEmailSettings();
        // create the mailer object
        $Mailer = \Swift_Mailer::newInstance($this->Transport);
        // set up debugging
        if($this->debug) {
            // create the logger plugin
            $Logger = new \Swift_Plugins_Loggers_EchoLogger();
            $Mailer->registerPlugin(new \Swift_Plugins_LoggerPlugin($Logger));
        }
        // send it
        try {
            //var_dump($this->Message);
            $sent_count = $Mailer->send($this->Message);
            // echo debugging results
            if($this->debug) {
                echo 'Sent ' . $sent_count . ' emails.<br />';
                $Logger->dump();
            }
            if(! $sent_count) {
                throw new \ErrorException('Failed to send any emails.');
            }
        } catch (\Exception $ex) {
            throw new \ErrorException('Email did not send. Details: ' . $ex->getMessage()); 
        }
        // success
        return $sent_count;
    }
    
    
    /**
     * Process the arrays of email addresses and add them to the swiftmailer object.
     * 
     * @return true
     * @throws \ErrorException
     * @version 11/12/2015 0.1.0
     * @since 11/12/2015 0.1.0
     */
    protected function processAddresses() {
        try {
            // to addresses
            if($this->to_addresses) {
                $this->Message->setTo($this->to_addresses);
            }
            // cc addresses
            if($this->cc_addresses) {
                $this->Message->setCC($this->cc_addresses);
            }
            // bcc addresses
            if($this->bcc_addresses) {
                $this->Message->setBCC($this->bcc_addresses);
            }
            // reply to addresses
            if($this->reply_to_addresses) {
                $this->Message->setReplyTo($this->reply_to_addresses);
            }
        } catch (\Exception $ex) {
            throw new \ErrorException('An error occured while setting the email addresses. Details: ' 
                    . $ex->getMessage() . ' File: ' . $ex->getFile() . ' Line: ' . $ex->getLine());
        }
        // success
        return true;
    }
    
    
}
